#!/usr/bin/env python
# -*- coding: utf-8 -*

import requests
import json



urlinitial = "https://bitbucket.org/api/2.0/repositories/git-m4u"
r = requests.get(urlinitial, auth=('usr_arquitetura', 'w@u5Pafr'))

json_string = r.json()
json_bitbucket = json.dumps(json_string)
j = json.loads(json_bitbucket)
total = 0
listaOfProjects=("zuum-mpayment","cielo-verde","plataformas","cielo-mobile","mpayment","vendasout","multirecarga","recarga-vivo","recarga-tim","subadquirente","recarga-oi","components","cielo","multirecarga-java-frontends","recorrente","recorrencia","recorrencia-tim","controle","pos","pos-terminal-m4u","gateway","interfaces","m4craft","omaha","claro","frontend")

for values in j["values"]:
    total = total + 1

    projeto_repo = values["name"].split('::', 1)[0]

    if projeto_repo not in listaOfProjects:
        print ('Fora do padrao :', values["name"],';<p>')

    #print projeto_repo
    #print values["name"]
next = j["next"]

while "next" in j:


        #print next
        r = requests.get(next, auth=('usr_arquitetura', 'w@u5Pafr'))

        json_string = r.json()
        json_bitbucket = json.dumps(json_string)
        k = json.loads(json_bitbucket)

        if "next" in k:
            next = k["next"]
        else:
            for values in k["values"]:

                total = total + 1
                projeto_repo = values["name"].split('::', 1)[0]

                if projeto_repo not in listaOfProjects:
                       print ('Fora do padrao :', values["name"],';<p>')

                #print projeto_repo
                #print values["name"]
            break

        tam = k["pagelen"]

        for values in k["values"]:

           total = total + 1
           projeto_repo = values["name"].split('::', 1)[0]

           if projeto_repo not in listaOfProjects:
                print ('Fora do padrao :', values["name"],';<p>')

           #print projeto_repo
           #print values["name"]
